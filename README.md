﻿# M183
Silvan Vogel

### 01_Keylogger
Ein Keylogger ist dazu da, die Eingaben eines Nutzers aufzuzeichnen, ohne dass er davon erfährt. 
So kann man an Informationen wie Benutzernamen und Passwörter herankommen. 
Normalerweise wird ein Keylogger mithilfe von XSS in eine Webseite eingefügt. 
Bei dieser Praxisarbeit haben wir dies aber einfach innerhalb eines selbst erstellen html files getan.

### 02_Clickjacking
Bei einer Clickjacking Attacke wird ein Fake Login-Formular für eine bestehende Seite detailgetreu (um es möglichst echt scheinen zu lassen) nachgemacht. 
Dieses Formular soll dann die Login Daten des Benutzers an den Hacker senden.
Bei dieser Praxisarbeit habe ich das login der Webseite 'www.wunderlist.com' nachgebaut 
und die echte webseite mit einem iframe im hintergrund eingebunden.

### 03_2FA
Bei der 2Fakor-Authentifizierung erweitert man ein Login mit einer zweiten (2 Faktor) Authentifizierung. 
Bei dieser Praxisarbeit haben wir das Login durch die Abfrage eines Timebased One Time Passwords (TOTP) erweitert.
Dazu habe ich ein Angular Projekt mit backend nodejs erstellt, welches die library speakeasy für die verifizierung verwendet.
Für die Initialisierung des TOTP kann z.B. Google Authenticator verwendet werden.

### 04_SSO
Diese Praxisarbeit mussten wir nicht machen, da wir für die IDPA in Berlin waren.

### 05_picoCTF
Bei dieser Praxisarbeit ging es darum mehrere kleine Aufgaben der picoCTF Challenge zu lösen.

### 04_password (Aufgabe 6)
Passwörter sollten sicher gespeichert werden, das heisst nicht einfach in plain text. Ebenfalls sollten sie z.B. mind. 8 Zeichen lang sein und Sonderzeichen enthalten.
Um dies zu machen, kann man Passwörter zB. Hashen oder Verschlüsseln.
Diese Praxisarbeit konnten wir im Team ausführen - sie ist bei Gian Furrer zu finden.
Wir haben uns entschieden das eingegebene Passwort wie Dropbox zu "speichern" (ausgeben).
Folgende Schritte durchläuft das Passwort:
1. Passwort muss bestimme Anforderungen erfüllen
2. PW wird mit SHA-256 gehasht + Salt
3. PW wird mit bcrypt gehasht 10x - verlangsamt den Vorgang
4. PW wird mit AES + Pepper Verschlüsselt

### 07_DigestAuth
HTTP Digest Authentication ist ein Bestandteil von HTTP und ist bereits im Browser implementiert. 
Es verwendet den MD5 Hash mit einem server generierten nonce (one time use number), um Wiederholungsangriffe wie Brute-force zu verhindern.
Die Abfolge geschieht wie folgt:
1. Der Client schick Anfrage an Server
2. Server sendet einen 401 'Unauthenticated' und im header einen nonce
3. Client kombiniert Passwort, nonce, uri usw. und schickt dies dann gehasht zurück an den Server. Der Server macht dasselbe und checkt ob es übereinstimmt

Da das passwort gehasht gesendet wird, muss der server nicht das wirkliche passwort (bsp: hallo) speichern, sondern nur den sog. digest (bsp: 2l3kKj3naN9).

### 10_Informationsquellen
Bei dieser Praxisarbeit haben wir verschiedene Informationsquellen zu den Themen Applikatonssicherheit, Informationssicherheit und Cyber Security untersucht.
Dabei sollten wir Fragen wie z.B. Der Nutzen dieser Quelle für persönliche/berufliche Zwecke, was es für einen Einfluss auf unser Verhalten hat oder unsere Erkenntnisse erklären. 
Ich habe dabei eine Sicherheitslücke in Whatsapp welche zu einem DOS führen kann, Real Time Phishing welches die 2 Faktor Authentifizierung umgehen soll, sowie das Thema Datenlecks recherchiert.
Vieles was ich gelernt habe steht im Dokument nicht geschrieben, da ich teilweise nicht wusste, wie ich es "kurz und knackig" beschreiben sollte.
Diese Praxisarbeit finde ich bisher fast am besten, da ich z.B. auch den Podcast "Darknet Diaries" angefangen habe zu hören und generell mein Interesse für die IT Sicherheit geweckt hat.

### 09_CSRFAttack - wurde nach Aufgabe 10 gelöst.
Bei dieser Praxisarbeit mussten wir eine Fake-Webseite erstellen, bei welcher user mit einer aktiven session id mit einer CSRF Attacke belästigt werden.
Anfangs dachte ich mir das es eine eher schwierige Arbeit wird. Nach anschauen der Requests und mehrfachem ausprobieren ist mir aber aufgefallen, 
dass wenn der CSRF Token nicht mitgesendet wird, dieser gar nicht validiert wird. Somit konnte ich die Aufgaben 2 & 3 relativ schnell lösen. 
Mit einer CSRF Attacke kann z.B. die Email oder das Password des Users geändert werden. Somit erhält man als Angreifer vollen Zugriff auf das Konto des Opfers.
Falls das Opfer über Admin Rechte verfügt, kann somit volle Kontrolle über das System übernommen werden.