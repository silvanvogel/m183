﻿### Kurzbeschreibung - 1
Quelle: https://www.heise.de/security/
Artikel: https://www.heise.de/security/meldung/Luecke-in-aelteren-WhatsApp-Versionen-erlaubte-Codeausfuehrung-aus-der-Ferne-4587119.html - 15.11.2019, 17:30 Uhr
Thema: Mit einer MP4-Datei zum DOS (Sicherheitslücke in Whatsapp) - Applikationssicherheit

Dieser Artikel hat für mich besonders persönlich eine Bedeutung, da ich sowie meine Familie und meine Freunde alle Whatsapp täglich benutzen.

Whatsapp ist eine verschlüsselte Nachrichten-Plattform. Diese wird von Privatpersonen, 
Unternehmen sowie auch Staatsengehörigen der ganzen Welt (vorallem Europa und USA) für den Nachrichten Austausch benutzt. 

Es hat sich herausgestellt, dass 'alte' Versionen (ca. 2 Monate und älter) eine Sicherheitslücke - mit der Kennzeichnung CVE-2019-11931 - enthalten die es erlaubt, Code aus der auszuführen, 
sowie auch das Auslösen eines Denial-of-Service des betreffenden Mobiltelefons. 
Ein Angreifer könnte dies ausnutzen, indem er eine speziell gestaltete MP4-Datei via Whatsapp verschickt, welche z.B. Schadhaften Code enthält.
Der Denial-of-Service kann ausgelöst werden, indem eine eine MP4-Datei an einen Whatsapp-Benutzer gesendet wird, welche einen Pufferüberlauf auslösen kann.

Damit könnten Systeme auf dem infizierten Gerät den Dienst verweigern oder wie erwähnt sogar Code aus der Ferne ausführen. 
Dies kann das Risiko darstellen, dass Malware auf einem infizierten Gerät, zum Abhören oder sogar einer Remote-Übernahme installiert wird.

Viele Benutzer haben "Automatische Updates" eingeschaltet oder machen es selbst regelmässig, weshalb ich denke dass dies kein grosses Risiko darstellt.
Trotzdem ist es interessant, sowie angsteinflössend, zu erfahren was man mit einem richtig präparierten MP4 File eigentlich anstellen kann. 
Soweit habe ich bisher noch nie gedacht, für mich war ein MP4 File ein einfaches Format für Videos mit Ton. 
Applikationen sollten also immer aktualisiert werden und auf dem neusten Stand sein!

### Kurzbeschreibung - 2
Quelle: https://www.heise.de/security/
Artikel: https://www.heise.de/newsticker/meldung/Datenleck-Kundendaten-von-Hotelbuchungsplattform-ungeschuetzt-im-Internet-4592945.html - 22.11.2019, 11:27 Uhr
Artikel 2: https://www.melani.admin.ch/melani/de/home/dokumentation/berichte/lageberichte/halbjahresbericht-2019-1.html - 22.11.2019, 12:18 Uhr
Thema: Datenleck

Bei einer Hotelbuchungsplattform wurde mehr als ein TByte an Kundendaten frei zugänglich auf einem Elasticsearch-Server gefunden. Darunter waren Buchungsinformationen, 
Kreditkartendetails sowie Zugangsdaten des Unternehmens.
Dabei waren Daten von Personen von überall aus Europa betroffen gewesen. 
Unter den Zugangs- und Zahlungsdaten hätten sich auch die der Weltgesundheitsorganisation befunden, damit wäre es möglich gewesen, Buchungen auf deren Kosten zu machen.

Es wurden keine Berichte über Missbrauch gemeldet. Solch ein Vorfall ist auch nichts einmaliges, andauernd hört man wieder, dass hier und da Daten frei zugänglich sind/waren.
Dasselbe passierte mit einem Elasticsearch-Server von Panama, auf diesem waren personenbezogene Daten von fast 90 Prozent der Einwohner. Die Daten welche öffentlich waren, waren Namen, 
Geburtsdaten, Ausweisnummern und andere persönliche Daten.
Auch da, ist nicht klar, ob jemand während dieser Zeit auf die ungeschützten Daten zugegriffen hat.

Ich habe mir immer Gedacht "Ist ja nicht schlimm wenn jemand an meine Daten kommt, was will er damit machen?". 
Dies zeigt mir aber wiedermal, dass man nicht so leichtsinnig mit den Daten, die man im Internet veröffenlicht, umgehen sollte.
Es zeigt auch, dass wenn man mit (v.a. sensiblen) Benutzerdaten zu tun hat, hat Sicherheit oberste Priorität!

### Kurzbeschreibung - 3
Quelle: https://www.melani.admin.ch/
Artikel: https://www.melani.admin.ch/melani/de/home/dokumentation/berichte/lageberichte/halbjahresbericht-2019-1.html - 22.11.2019, 13:20 Uhr
Thema: Real Time Phishing

Real Time Phishing soll einem Hacker Zugang zu einem (beispielsweise) Bankkonto, trotz OTP, verschaffen. Real Time Phishing funktioniert gleich, wie normales phishing, 
einfach dass der Hacker aktiv wird, sobald das Opfer auf den Link zu seiner gefälschten Seite klickt. Der Vorgang geschieht wie folgt:

1. Opfer klickt auf Link in E-Mail und wird redirected
2. Opfer gibt Benutzername (oder Kontonummer usw.) und Passwort ein. --> Hacker erhält Login Daten
3. Opfer gibt OTP ein und bekommt Fehlermeldung --> Hacker hat OTP und ist somit im Konto des Benuters eingeloggt

Dies zeigt, dass nicht nur die Sicherheit steigt, sondern Hacker auch immer neue Wege finden um an Daten ranzukommen. 
Vorallem für Banken ist dies verheerend, die Sicherheit der 2 Faktor Authentifizierung kann mit dieser Methode einfach umgangen werden.
Wenn man damit ein bisschen vertraut ist, ist dies aber sehr einfach zu verhindern. Man muss einfach sicherstellen, 
dass die Webseite Offiziell ist und sollte sich nicht über irgendwelche 'komischen' Links anmelden.

Dieses Verfahren kannte ich bisher noch nicht. Es ist aber nicht viel anders als normales Phishing und kostet den Hacker nicht viel, 
ausser Zeit, da er bereit sein muss wenn sich ein Opfer anmelden will.