const express = require('express');
const storage = require('./storage');
const router = express.Router();

// Register new User or reset the existing user information (only one login is possible)
router.post('/register', (req, res) => {
    const result = req.body;

    // Check for data and remove whitespaces
    if ((!result.username && !result.password) || (result.username.trim() == "" || result.password.trim() == "")) {
        return res.send({
            "message": "Username/password is required"
        });
    }

    storage.user.username = result.username;
    storage.user.password = result.password;
    delete storage.user.tfa;

    return res.send({
        "status": 200,
        "message": "You have successfully registered"
    });
});

// Export for app.js
module.exports = router;