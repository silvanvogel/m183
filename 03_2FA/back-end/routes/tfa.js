const express = require('express');
const speakeasy = require('speakeasy');
const QRCode = require('qrcode');
const storage = require('./storage');
const router = express.Router();

// Setup 2fa
router.post('/tfa/setup', (req, res) => {
    // generate key
    const secret = speakeasy.generateSecret({
        length: 10,
        name: storage.user.username,
        issuer: 'Silvan 2FA'
    });


    var url = speakeasy.otpauthURL({
        secret: secret.base32,
        label: storage.user.username,
        encoding: 'base32'
    });

    // Get the data url of the authenticator
    QRCode.toDataURL(url, (err, dataURL) => {
        storage.user.tfa = {
            secret: '',
            tempSecret: secret.base32,
            dataURL,
            tfaURL: url
        };
        return res.json({
            message: '2FA Auth needs to be verified',
            tempSecret: secret.base32,
            dataURL,
            tfaURL: secret.otpauth_url
        });
    });
});

// Get 2FA Details
router.get('/tfa/setup', (req, res) => {
    res.json(storage.user.tfa ? storage.user.tfa : null);
});

// 2fa verification
router.post('/tfa/verify', (req, res) => {
    // verify the given token (returns true if tokens match)
    let isVerified = speakeasy.totp.verify({
        secret: storage.user.tfa.tempSecret,
        encoding: 'base32',
        token: req.body.token
    });

    if (isVerified) {
        storage.user.tfa.secret = storage.user.tfa.tempSecret;
        return res.send({
            "status": 200,
            "message": "Two-factor Authentication is enabled successfully"
        });
    }

    return res.send({
        "message": "Invalid Authentication Code, verification failed."
    });
});

// Export for app.js
module.exports = router;