const express = require('express');
const speakeasy = require('speakeasy');
const storage = require('./storage');
const router = express.Router();

// Login storage.user
router.post('/login', (req, res) => {
    // Check if the user registered (if there is any data stored)
    if (storage.user.username && storage.user.password) {
        // Check if username and password are (in)correct
        if(req.body.username != storage.user.username || req.body.password != storage.user.password){
            return res.send({
                "status" : 403,
                "message": "Invalid username or password"
            });
        }

        // Check if 2fa is already existing
        if (!storage.user.tfa || !storage.user.tfa.secret) {
            return res.send({
                "status": 200,
                "message": "success"
            });
        } 
        else {
            // check if 'x-2fa' header is existing 
            if (req.headers['x-2fa']) {
                // verify secret key
                let isVerified = speakeasy.totp.verify({
                    secret: storage.user.tfa.secret,
                    encoding: 'base32',
                    token: req.headers['x-2fa']
                });

                if (isVerified) {
                    return res.send({
                        "status" : 200,
                        "message": "success"
                    });
                } else {
                    return res.send({
                        "status" : "invalidAuth",
                        "message": "Invalid Authentication Code"
                    });
                }
            } else {
                return res.send({
                    "status": "invalidAuth",
                    "message": "Please enter the Authentication Code"
                });
            }
        }
    }

    return res.send({
        "status": 403,
        "message": "Please register to login"
    });
});

// Export for app.js
module.exports = router;