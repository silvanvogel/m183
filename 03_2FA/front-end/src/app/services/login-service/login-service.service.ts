import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable()
export class LoginServiceService {
  headerOptions: any = null
  isLoggedIn: boolean = false
  authSub = new Subject<any>();

  constructor(private http: HttpClient) {
  }

  // Login User
  loginAuth(user: any) {
    if (user.authcode) {
      // Append authentication code to header
      this.headerOptions = new HttpHeaders({
        'x-2fa': user.authcode
      });
    }
    return this.http.post("http://localhost:3000/login", { username: user.username, password: user.password }, { observe: 'response', headers: this.headerOptions });
  }

  // Setup 2FA 
  setupAuth() {
    return this.http.post("http://localhost:3000/tfa/setup", {}, { observe: 'response' })
  }

  // Register the new user
  registerUser(user: any) {
    return this.http.post("http://localhost:3000/register", { username: user.username, password: user.password }, { observe: "response" });
  }

  // Update 'logged in' status
  updateAuthStatus(value: boolean) {
    this.isLoggedIn = value
    this.authSub.next(this.isLoggedIn);
    // Set 'logged in' status in local storage
    localStorage.setItem('isLoggedIn', value ? "true" : "false");
  }

  // Check if user is loggedin
  getAuthStatus() {
    this.isLoggedIn = localStorage.getItem('isLoggedIn') == "true" ? true : false;
    return this.isLoggedIn
  }

  // Logout the user
  logoutUser() {
    this.isLoggedIn = false;
    this.authSub.next(this.isLoggedIn);
    // Set 'logged in' status in local storage
    localStorage.setItem('isLoggedIn', "false")
  }

  // Get 2FA
  getAuth() {
    return this.http.get("http://localhost:3000/tfa/setup", { observe: 'response' });
  }

  // Verify 2FA
  verifyAuth(token: any) {
    return this.http.post("http://localhost:3000/tfa/verify", { token }, { observe: 'response' });
  }
}