import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { LoginServiceService } from '../../services/login-service/login-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css', '../../../custom.css']
})
export class LoginComponent implements OnInit {
  tfaFlag: boolean = false

  user = {
    username: "",
    password: "",
    authcode: ""
  }
  
  errorMessage: string = ""

  constructor(private loginService: LoginServiceService, private router: Router) { }

  ngOnInit() {
  }

  // Call loginUser function in login-service
  loginUser() {
    this.loginService.loginAuth(this.user).subscribe((data) => {
      // Set error message to empty, if he already tried logging in
      this.errorMessage = "";

      if (data.body['status'] === 200) {
        this.loginService.updateAuthStatus(true);
        this.router.navigateByUrl('/tfa');
      }
      if (data.body['status'] === "invalidAuth") {
        this.tfaFlag = true;
      }
      if (data.body['status'] === 403) {
        this.errorMessage = data.body['message'];
      }
    })
  }
}
