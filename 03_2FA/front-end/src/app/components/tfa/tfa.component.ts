import { Component, OnInit } from '@angular/core';
import { LoginServiceService } from '../..//services/login-service/login-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tfa',
  templateUrl: './tfa.component.html',
  styleUrls: ['./tfa.component.css', '../../../custom.css']
})
export class TfaComponent implements OnInit {
  tfa: any = {};
  authcode: string = "";
  errorMessage: string = "";

  constructor(private loginService: LoginServiceService, private router: Router) {
    this.getAuthDetails();
   }

  ngOnInit() {
  }

  // Get 2FA Details or setup 2FA
  getAuthDetails() {
    this.loginService.getAuth().subscribe((data) => {
      const result = data.body
      if (data['status'] === 200) {
        if (result == null) {
          this.setup();
        } else {
          this.tfa = result;
          this.router.navigate(['/home']);
        }
      }
    });
  }

  // Setup 2FA
  setup() {
    this.loginService.setupAuth().subscribe((data) => {
      const result = data.body
      if (data['status'] === 200) {
        this.tfa = result;
      }
    });
  }

  // Verify the 2FA
  confirm() {
    this.loginService.verifyAuth(this.authcode).subscribe((data) => {
      const result = data.body
      if (result['status'] === 200) {
        this.errorMessage = "";

        this.tfa.secret = this.tfa.tempSecret;
        this.tfa.tempSecret = "";
      } else {
        this.errorMessage = result['message'];
        this.router.navigate(['/home']);
      }
    });
  }
}