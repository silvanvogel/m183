import { Component, OnInit } from '@angular/core';
import { LoginServiceService } from '../../services/login-service/login-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css', '../../../custom.css']
})
export class RegisterComponent implements OnInit {
  errorMessage: string = null

  user = {
    username: "",
    password: ""
  }

  confirmPassword: string = ""

  constructor(private loginService: LoginServiceService, private router: Router) { }

  ngOnInit() {
  }

  // call registerUser functin in login-service
  registerUser() {
    if (this.user.password.trim() !== "" && (this.user.password.trim() === this.confirmPassword))
      this.loginService.registerUser(this.user).subscribe((data) => {
        const result = data.body
        
        if (result['status'] === 200) {
          this.errorMessage = result['message'];
          this.router.navigate(['/login']);
        }
      });
  }
}
