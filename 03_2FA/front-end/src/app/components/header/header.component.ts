import { Component, OnInit } from '@angular/core';
import { LoginServiceService } from '../../services/login-service/login-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css', '../../../custom.css']
})
export class HeaderComponent implements OnInit {
  isLoggedIn: boolean = false

  constructor(private loginService: LoginServiceService, private router: Router) {
    this.loginService.authSub.subscribe((data) => {
      this.isLoggedIn = data
    })
  }

  // Check if user is logged in
  ngOnInit() {
    this.isLoggedIn = this.loginService.getAuthStatus()
  }

  // toggle doesn't work with bootstrap js...
  toggleMenuBar() {
    if(document.getElementById("collapsibleNavId").style.display == "block") {
      document.getElementById("collapsibleNavId").style.display = "none";
    } else {
      document.getElementById("collapsibleNavId").style.display = "block";
    }
  }

  // Logout user
  logout() {
    this.loginService.logoutUser()
    this.router.navigate(['/login'])
  }
}
